extends Area3D

@export var light: SpotLight3D

var default_energy

# Called when the node enters the scene tree for the first time.
func _ready():
	await get_tree().physics_frame
	if light == null:
		return
	default_energy = light.light_energy
	light.light_energy = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func switch():
	light.visible = default_energy

func switch_back():
	light.visible = 0
