extends Control

@onready var fps_label = $FPS

func _process(delta):
	if self.visible == false:
		return
	var fps = Engine.get_frames_per_second()
	fps_label.text = "FPS: " + str(fps)
