extends SwitchComponent
class_name Door

var initial_rotation
var current_rotation
var open: bool = false
var moving = false

@export var locked = false
@export var locked_sound: AudioStream
@export var unlocked_sound: AudioStream

@onready var anim_player = $AnimationPlayer
@onready var audio_player = $AudioStreamPlayer3D

func _ready():
	current_rotation = rotation_degrees.y
	initial_rotation = current_rotation

func trigger():
	if locked == true:
		audio_player.stream = locked_sound
		audio_player.play()
		return
	#rotation.y = lerpf(current_rotation, current_rotation - 90, 1.0)+
	moving = true
	match open:
		true:
			#print("door open, now close")
			#anim_player.play("close")
			rotation_degrees.y -= 90
		false:
			#print("door close, now open")
			#anim_player.play("open")
			rotation_degrees.y += 90
	open = !open
	pass

func unlock():
	audio_player.stream = unlocked_sound
	audio_player.play()
	locked = false

func _process(delta):
	if moving == true:
		pass
		rotation.y
