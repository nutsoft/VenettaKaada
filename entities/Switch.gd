@tool
extends Node3D
class_name Switch

@export var mesh: Mesh
@export var default_material: StandardMaterial3D
@export var triggered_material: StandardMaterial3D
@export var interact_radius: float
#@export var interact_shape
#@export var target: SwitchComponent
@export var target: Node3D
@export var interact_name: String
@export var oneshot: bool = true
@export var door_unlocker = false

@onready var mesh_instance = $MeshInstance3D
@onready var interaction_area = $InteractionArea
@onready var interaction_shape = $InteractionArea/CollisionShape3D

var used = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	mesh_instance.mesh = mesh
	mesh_instance.material_override = default_material
	interaction_area.interact = Callable(self, "_on_interact")
	if interact_name != null:
		interaction_area.name = interact_name
	if interact_radius != 0:
		interaction_shape.radius = interact_radius


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_interact():
	if oneshot == true and used > 0:
		return
	mesh_instance.material_override = triggered_material
	if target == null:
		return
	if door_unlocker == true:
		target.unlock()
		return
	target.trigger()
